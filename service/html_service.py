from bs4 import BeautifulSoup
from service.json_data_service import JsonDataService


class HtmlDataService:
    @staticmethod
    def write_to_html():
        data = JsonDataService.read_all_from_json_file()

        html_doc = BeautifulSoup(
            '<html> <head><title>Servers</title></head> <body></body> </html>',
            "html.parser")
        body = html_doc.find("body")

        for server_id, server_info in data.items():
            server_name = server_info["name"]
            server_ip = server_info["ip"]
            server_status = "Online" if server_info["status"] else "Offline"
            last_check = server_info["last_check"]

            server_html = (f"<p><strong>Name:</strong> {server_name}<br><strong>IP:</strong> {server_ip}"
                           f"<br><strong>Status:</strong> {server_status}<br><strong>Last Check:</strong>"
                           f" {last_check}</p>")

            body.append(BeautifulSoup(server_html, "html.parser"))

        with open("servers.html", "w") as html_file:
            html_file.write(str(html_doc))

import json
from domain.server_model import Server


class JsonDataService:
    @staticmethod
    def write_to_json_file(result_dict, json_file="servers.json") -> None:
        with open(json_file, "w") as f:
            json.dump(result_dict, f, indent=4, default=lambda o: o.__dict__)

    @classmethod
    def read_all_from_json_file(cls, json_file="servers.json"):
        with open(json_file, "r") as f:
            return json.load(f)

    @classmethod
    def add_server(cls, name, ip) -> None:
        servers = cls.read_all_from_json_file()
        servers.update(Server(name, ip).to_dict())
        cls.write_to_json_file(servers)

    @classmethod
    def remove_server(cls, index) -> None:
        servers = cls.read_all_from_json_file()
        i = 0
        for k, v in servers.items():
            print(i, k, v)
            if i == index:
                servers.pop(k)
                break
            i += 1

        cls.write_to_json_file(servers)

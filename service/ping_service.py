import platform
import re
import subprocess
import time
from rich import print


class PingService:
    @staticmethod
    def ping(server) -> int:
        ip = server['ip']
        if ip is None or ip == "" or not PingService.is_ipv4_valid(ip):
            print(f"Invalid IP: {ip}")
            return 1

        param = f"-n " if platform.system().lower() == "windows" else "-c "
        num_pings: int = 3
        param += str(num_pings)

        return subprocess.call(
            f"ping {param} {ip}",
            shell=True,
            stdout=None  # =subprocess.DEVNULL to silence
        )

    @staticmethod
    def is_ipv4_valid(ip: str) -> bool:
        ipv4_pattern = re.compile(r"^"
                                  r"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\."
                                  r"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\."
                                  r"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\."
                                  r"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)"
                                  r"$")

        return re.match(ipv4_pattern, ip) is not None

    @staticmethod
    def ping_all(servers: dict) -> dict:
        for key, server in servers.items():
            server['status'] = True if PingService.ping(server).real == 0 else False
            server['last_check'] = time.time()
        return servers

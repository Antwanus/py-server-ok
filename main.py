#!/usr/bin/python3
import os
import sys
import time
from service.html_service import HtmlDataService
from service.json_data_service import JsonDataService
from service.ping_service import PingService
from rich import print


def main() -> None:
    if len(sys.argv) > 1:
        match sys.argv[1]:
            case "ping":
                JsonDataService.write_to_json_file(
                    PingService.ping_all(
                        JsonDataService.read_all_from_json_file()
                    )
                )
                HtmlDataService.write_to_html()
            case "help":
                print("Usage: python3 main.py [ping|help]")
                print(" ping: ping all servers")
                print(" help: print this help")
            case _:
                print("Invalid argument: ", sys.argv[1])
                print("Usage: python3 main.py [ping|help]")
                print(" ping: ping all servers")
                print(" help: print this help")
        return
    # else run the interactive mode
    should_run = True
    while should_run:
        os.system("clear")
        print("############### [bold magenta]SERVER-OK[/bold magenta] ###################")
        print("#                                           #")
        print("#        [bold red]Welcome home commander![/bold red]             #")
        print("#                                           #")
        print("#   1 -> List all servers                   #")
        print("#   2 -> Add a server                       #")
        print("#   3 -> Remove a server                    #")
        print("#   4 -> Ping all servers                   #")
        print("#   * -> Exit the application               #")
        print("#                                           #")
        print("#############################################")

        choice = input("Enter your choice: ")[0]
        match choice:
            case "1":
                print("\t--- [bold magenta]List all servers[/bold magenta] ---")
                print_all_servers()
                input("\nPress enter to continue")
            case "2":
                print("--- [bold magenta]Add a server[/bold magenta] --- ")
                name = input("Enter the name of the server: ")
                ip = input("Enter the IP of the server: ")
                while not PingService.is_ipv4_valid(ip):
                    print("Invalid IP", ip)
                    ip = input("Enter the IP of the server: ")

                JsonDataService.add_server(name, ip)
                HtmlDataService.write_to_html()
                print("Server added successfully")
                input("\nPress enter to continue")
            case "3":
                print("[bold magenta]--- Remove a server ---[/bold magenta]")
                print_all_servers()
                choice = input("Enter the index of the server to remove (or nothing to go back): ")
                if choice == "":
                    continue
                try:
                    index = int(choice)
                except ValueError:
                    print("Invalid index")
                    continue
                JsonDataService.remove_server(index)
                HtmlDataService.write_to_html()

                input("\nPress enter to continue")
            case "4":
                print("[bold magenta]--- Ping all servers ---[/bold magenta]")
                JsonDataService.write_to_json_file(
                    PingService.ping_all(
                        JsonDataService.read_all_from_json_file()
                    )
                )
                HtmlDataService.write_to_html()

                input("\nPress enter to continue")
            case _:
                print("Exit")
                should_run = False


def print_all_servers():
    i = 0
    for value in JsonDataService.read_all_from_json_file().values():
        str_builder = (f"[bold magenta]{i}[/bold magenta]\tName: {value['name']}"
                       f"\n\tIP: {value['ip']}"
                       f"\n\tStatus: {value['status']} "
                       f"\n\tLast check: {time.ctime(value['last_check'])}")
        print(str_builder)
        i += 1


if __name__ == "__main__":
    main()

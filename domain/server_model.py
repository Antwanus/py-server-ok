import hashlib
import time


class Server:
    def __init__(self, name: str, ip: str):
        self.id = hashlib.sha256(ip.encode('utf-8')).hexdigest()
        self.name = name
        self.ip = ip
        self.status = False
        self.last_check = time.time()

    def __repr__(self):
        return f"{{" \
               f"name: {self.name}, " \
               f"ip: {self.ip}, " \
               f"status: {self.status}, " \
               f"last_check: {self.last_check}" \
               f"}}"

    def to_dict(self):
        return {
            self.id: {
                "name": self.name,
                "ip": self.ip,
                "status": self.status, "last_check": self.last_check}
        }
